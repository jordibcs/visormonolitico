package pe.com.test.system.testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MantenimientoProductoTest {

	private WebDriver driver;
	private String urlInicial = "http://localhost:8080/VisorWeb/";
	
	@BeforeTest
	public void setUp()throws Exception{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Jordi\\Documents\\UPC\\Ciclo7\\DiseñoDeExperimentosDeISW\\DE\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
	}
	
	//POM=Page Object Model
			@Test
			public void insertarProducto_FlujoBasico()throws Exception {		
				try {
					String mensajeEsperado="Se guardó de manera correcta el Producto";
					driver.get(urlInicial);
					driver.findElement(By.id("txtUsuario")).clear();
					driver.findElement(By.id("txtUsuario")).sendKeys("admin");
					driver.findElement(By.id("txtClave")).clear();
					driver.findElement(By.id("txtClave")).sendKeys("clave");
					driver.findElement(By.id("btnIniciarSesion")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("/html/body/section/div[1]/div")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("/html/body/section/div[1]/nav/ul/li/span")).click();
					Thread.sleep(2000);
					driver.findElement(By.linkText("Mnt. de Productos")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("btnNuevo")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("txtNombre")).clear();
					driver.findElement(By.id("txtNombre")).sendKeys("Producto Test");
					driver.findElement(By.id("txtPrecio")).clear();
					driver.findElement(By.id("txtPrecio")).sendKeys("Producto Test");
					driver.findElement(By.id("btnGuardar")).click();
					Thread.sleep(2000);
					
					String mensajeObtenido=driver.findElement(By.id("messages")).getText();
					Assert.assertEquals(mensajeObtenido, mensajeEsperado);
					Thread.sleep(2000);
					
				} catch (Exception e) {
					e.printStackTrace();
					Assert.fail();
				}
			}
			
			@Test
			public void insertarProducto_FlujoAlternativo()throws Exception {
			}
			
			
			@AfterTest
			public void tearDown() throws Exception{
				driver.close();
			}
}
