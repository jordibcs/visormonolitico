Feature: Mantenimietno de Producto
  Como jefe de almacen necesito realizar la gestión de Productos
	
  Scenario: Registrar Producto
    Given despues de iniciar sesion en la aplicacion
    When 	hago click en el enlace de Mantenimiento de Producto
    And 	luego hago click en el boton de Nuevo
    And 	en la nueva pantalla escribo en campo Nombre el valor de "Yogurt"
    And 	en la nueva pantalla escribo en campo Precio el valor de "2.00"
    And 	en la nueva pantalla selecciono en campo Categoria el valor de "Lacteos"
    And 	presiono el boton de Guardar 
    Then 	el sistema me muestra el mensaje de: "Se guardo correctamente el Producto"
		
  Scenario: Actualizar Producto
    Given despues de iniciar sesion en la aplicacion
    When 	hago click en el enlace de Mantenimiento de Producto
    And 	busco el producto "Yogurt" para seleccionarlo de la tabla de Productos
    And 	luego hago click en el boton de Editar
    And 	en la nueva pantalla escribo en campo Nombre el valor de "Queso" 
    And 	presiono el boton de Actualizar
    Then 	el sistema me muestra el mensaje de: "Se actualizo correctamente el Producto"
    
  Scenario: Eliminar Producto
    Given despues de iniciar sesion en la aplicacion
    When 	hago click en el enlace de Mantenimiento de Producto
    And 	busco el producto "Queso" para seleccionarlo de la tabla de Productos
    And 	luego hago click en el boton de Eliminar
    Then 	el sistema me muestra el mensaje de: "Se elimino correctamente el Producto"	