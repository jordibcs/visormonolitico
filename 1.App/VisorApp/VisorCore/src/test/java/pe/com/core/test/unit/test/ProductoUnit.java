package pe.com.core.test.unit.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.mockito.Mock;

import pe.com.core.dao.ProductoDao;
import pe.com.core.entity.Producto;

public class ProductoUnit {

	@Mock
	private ProductoDao productoDao;
	@Mock
	private Producto producto;
	@Test
	public void testCP01() {
		producto = new Producto();
		producto.setIdCategoria(1);
		producto.setNombre("Sublime");
		producto.setPrecio(2.00);
		producto.setIdProducto(1);
		try {
			assertTrue(productoDao.insertar(producto).equals(producto));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCP02() {
		producto = new Producto();
		producto.setIdCategoria(1);
		producto.setNombre("");
		producto.setPrecio(2.00);
		producto.setIdProducto(1);
		try {
			assertTrue(productoDao.insertar(producto).equals("Nombre: Error de validación: se necesita un valor."));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCP03() {
		producto = new Producto();
		producto.setIdCategoria(1);
		producto.setNombre("Sublime");
		producto.setPrecio(-2.00);
		producto.setIdProducto(1);
		try {
			assertTrue(productoDao.insertar(producto).equals(producto));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCP04() {
		producto = new Producto();
		producto.setIdCategoria(1);
		producto.setNombre("Sublime");
		producto.setPrecio(null);
		producto.setIdProducto(1);
		try {
			assertTrue(productoDao.insertar(producto).equals("Precio: Error de validación: se necesita un valor."));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
