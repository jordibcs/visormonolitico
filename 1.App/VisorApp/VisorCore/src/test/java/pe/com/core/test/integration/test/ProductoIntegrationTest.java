package pe.com.core.test.integration.test;

import static org.mockito.Mockito.doNothing;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pe.com.core.business.ProductoBusiness;
import pe.com.core.entity.Categoria;
import pe.com.core.entity.Producto;

public class ProductoIntegrationTest {
	private final ProductoBusiness productoBusiness = new ProductoBusiness();
	private static Producto producto = new Producto();
	private String mensaje = "";
	private String nombreProducto = "";
	private String precioProducto = "";
	private int idCategoria = 0;
	@Mock
	private HttpServletResponse response;
	
	@Given("^	despues de iniciar sesion en la aplicacion$")
    public void despues_de_iniciar_sesion_en_la_aplicacion() throws Throwable {
		MockitoAnnotations.initMocks(this);
		doNothing().when(response).sendRedirect("http://master/VisorWeb");
		Assert.assertTrue(true);
    }

    @When("^	hago click en el enlace de Mantenimiento de Producto$")
    public void hago_click_en_el_enlace_de_mantenimiento_de_producto() throws Throwable {
    	doNothing().when(response).sendRedirect("http://master/mntAdmProducto.xhtml");
		Assert.assertTrue(true);
    }

    @Then("^	el sistema me muestra el mensaje de: \"([^\"]*)\"$")
    public void el_sistema_me_muestra_el_mensaje_de_something(String strArg1) throws Throwable {
    	try {
			Assert.assertEquals(strArg1, mensaje);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Error: " + e.getMessage());
		}
    }

    @And("^	en la nueva pantalla escribo en campo Precio el valor de \"([^\"]*)\"$")
    public void en_la_nueva_pantalla_escribo_en_campo_precio_el_valor_de_something(String arg1) throws Throwable {
        precioProducto = arg1;
        producto.setPrecio(Double.valueOf(precioProducto));
    }

    @And("^	en la nueva pantalla selecciono en campo Categoria el valor de \"([^\"]*)\"$")
    public void en_la_nueva_pantalla_selecciono_en_campo_categoria_el_valor_de_something(String strArg1) throws Throwable {
        idCategoria = Integer.valueOf(strArg1);
    	producto.setIdCategoria(Integer.valueOf(idCategoria));
    }

    @And("^	presiono el boton de Guardar $")
    public void presiono_el_boton_de_guardar() throws Throwable {
    	try {
			productoBusiness.insertar(producto);
			mensaje = "Se guardo correctamente el Producto";
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Error: " + e.getMessage());
		}
    }

    @And("^	busco el producto \"([^\"]*)\" para seleccionarlo de la tabla de Productos$")
    public void busco_el_producto_something_para_seleccionarlo_de_la_tabla_de_productos(String strArg1) throws Throwable {
    	try {
			List<Producto> lista = productoBusiness.listar(strArg1);
			Assert.assertTrue(lista.size() > 0);
			producto = lista.get(0);
			doNothing().when(response).sendRedirect("http://master/mntAdmProducto.xhtml");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Error: " + e.getMessage());
		}
    }

    @And("^	luego hago click en el boton de Editar$")
    public void luego_hago_click_en_el_boton_de_editar() throws Throwable {
    	doNothing().when(response).sendRedirect("http://master/mntAdmProducto.xhtml");
		Assert.assertTrue(true);
    }

    @And("^	en la nueva pantalla escribo en campo Nombre el valor de \"([^\"]*)\" $")
    public void en_la_nueva_pantalla_escribo_en_campo_nombre_el_valor_de_something(String strArg1) throws Throwable {
        nombreProducto = strArg1;
        producto.setNombre(nombreProducto);
    }

    @And("^	presiono el boton de Actualizar$")
    public void presiono_el_boton_de_actualizar() throws Throwable {
    	try {
			productoBusiness.actualizar(producto);
			mensaje = "Se actualizo correctamente el Producto";
			List<Producto> lista = productoBusiness.listar(nombreProducto);
			Assert.assertTrue(lista.size() > 0);
			Assert.assertEquals(lista.get(0).getNombre().toUpperCase(), producto.getNombre().toUpperCase());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Error: " + e.getMessage());
		}
    }

    @And("^	luego hago click en el boton de Eliminar$")
    public void luego_hago_click_en_el_boton_de_eliminar() throws Throwable {
    	try {
			productoBusiness.eliminar(producto);
			mensaje = "Se elimino correctamente el Producto";
			Assert.assertTrue(true);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Error: " + e.getMessage());
		}
    }
}
